package synonimus;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Synonyms {

    private static int testCases = -1;
    private static int dictionarySize = 0;
    private static Set<String[]> dictionary = new HashSet<>();
    private static int dataSize = 0;
    private static String analysisResult = "";
    private static boolean analyse = true;

    public static void main(String[] args) {
        LoadAndAnalyseTheData();
    }

    private static void LoadAndAnalyseTheData() {

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "c:\\Test\\test.in"));
            String line = reader.readLine();
            testCases = Integer.parseInt(line);
            while (line != null) {
                line = reader.readLine();
                if (line != null) {
                    String[] lineContent = line.toLowerCase().split(" ");
                    if (lineContent.length > 1) {
                        handleNonDigitLine(lineContent);
                    } else {
                        handleDigitLine(Integer.parseInt(line));
                    }
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (testCases == 0) {
            saveAnalysis();
        }

    }

    private static void handleDigitLine(int number) {
        analyse =! analyse;
        if (!analyse) {
            dictionarySize = number;
            resetData();
            testCases--;
        } else {
            dataSize = number;
        }
    }

    private static void handleNonDigitLine(String[] lineContent) {
        if (analyse && dataSize > 0) {
            analysisResult += analyseEntry(lineContent) +"\n";
            dataSize--;
        } else {
            if (dictionarySize > 0) {
                dictionary.add(lineContent);
                dictionarySize--;
            }
        }
    }

    private static String analyseEntry(String[] entry) {
        return (entry[0].equals(entry[1]) || anySynonymsOrderMatch(entry) || checkSynonymsIndirectly(entry) || checkSynonymsIndirectly(reverseEntry(entry))) ? "synonyms" : "different";
    }

    private static boolean anySynonymsOrderMatch(String[] entry) {
        return dictionary.stream().anyMatch(dict -> (dict[0].equals(entry[0]) && dict[1].equals(entry[1])) || (dict[0].equals(entry[1]) && dict[1].equals(entry[0])));
    }

    private static boolean checkSynonymsIndirectly(String[] entry) {
        Set<String> directSynonyms = dictionary.stream().filter(f -> f[0].equals(entry[0])).map(m -> m[1]).collect(Collectors.toSet());

        Set<String> indirectSynonyms = new HashSet<>();
        dictionary.forEach(d -> {
            directSynonyms.forEach(ds -> {
                if (ds.equals(d[0])) {
                    indirectSynonyms.add(d[1]);
                }
            });
        });

        return new HashSet<>(Arrays.asList(entry)).stream().anyMatch(indirectSynonyms::contains);
    }

    private static String[] reverseEntry(String[] entry) {
        String[] copiedEntries = new String[2];
        copiedEntries[0] = entry[1];
        copiedEntries[1] = entry[0];
        return copiedEntries;
    }

    private static void resetData() {
        dictionary = new HashSet<>();
    }

    private static void saveAnalysis() {
        File file = new File("c:\\Test\\test.out");
        try {
            FileWriter fw = new FileWriter(file, file.exists());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(analysisResult);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
